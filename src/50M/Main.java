import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {

    private static final String URL =
            "jdbc:postgresql://course-db.csqiiocqatup.us-east-1.rds.amazonaws.com:5432/postgres";
    private static final String USER = "postgres";
    private static final String PASSWORD = "sH28uu9oo#";

    // language=SQL
    private static final String SQL_INSERT_50M = "insert into names(id, name) values (?, ?)";

    private static Connection connection;
    private static RandomInRange<Integer> iRandom;
    private static Random rand = new Random();
    private static Function<Integer, String> strRandomGenerator;

    static {
        iRandom = (lower, upper) -> lower.intValue()
                + rand.nextInt(upper.intValue() - lower.intValue());

        strRandomGenerator =
                length -> { Character[] arr = new Character[length];
                    IntStream.range(0, length)
                            .forEach(x -> arr[x] = (char)(iRandom.apply((int)'a', (int)'z').intValue()));
                    return Arrays.stream(arr).map(String::valueOf)
                            .collect(Collectors.joining(""));
                };
    }


    public static void insertInto50M () {
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SQL_INSERT_50M);
            for (int i = 1; i <= 5e7; i++) {
                preparedStatement.setInt(1, (int) i);
                preparedStatement.setString(2, strRandomGenerator.apply(5));
                preparedStatement.addBatch();
                if (i % (int) 5e4 == 0) preparedStatement.executeBatch();
            }
            preparedStatement.executeBatch();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (preparedStatement != null) try { preparedStatement.close(); }
            catch (SQLException e) {}
        }

    }

    public static void main(String[] args) throws SQLException, IOException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        connection = DriverManager.getConnection(URL, USER, PASSWORD);
        insertInto50M();
    }

}