//10.0
CREATE TABLE A(
        id bigint,
        CONSTRAINT a_pk PRIMARY KEY (id)
);

CREATE TABLE B(
        id bigint,
        par_id bigint,
        CONSTRAINT b_pk PRIMARY KEY (id),
        CONSTRAINT a_fk FOREIGN KEY (par_id) REFERENCES A(id)
);

//10.1
DO $$
BEGIN
        FOR i IN 0..99999 LOOP
                INSERT INTO a(id) VALUES(i);
        END LOOP;
        FOR i IN 0..499999 LOOP
                INSERT INTO b(id, par_id) VALUES(i, i / 5);
        END LOOP;
END$$;

//10.2
CREATE INDEX b_fk__index ON b(par_id);
