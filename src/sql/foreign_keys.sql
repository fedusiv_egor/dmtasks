//9.0
ALTER TABLE countries ADD PRIMARY KEY(ID);

//9.2
ALTER TABLE citizens ADD FOREIGN KEY(Country_ID) REFERENCES countries(ID);
ALTER TABLE capitals ADD FOREIGN KEY(Country_ID) REFERENCES countries(ID);

//9.3
ALTER TABLE citizens drop CONSTRAINT citizens_country_id_fkey;
ALTER TABLE capitals drop CONSTRAINT capitals_country_id_fkey;

//9.4
ALTER TABLE citizens ADD CONSTRAINT country_fk FOREIGN KEY(Country_ID) REFERENCES countries(ID)
ON DELETE CASCADE;
ALTER TABLE capitals ADD CONSTRAINT country_fk FOREIGN KEY(Country_ID) REFERENCES countries(ID)
ON DELETE CASCADE;