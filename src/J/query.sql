with _t1 as (
        select * from dna1
        intersect all
        select * from dna2
    ),

    _t2 as (
        select * from dna1
        union all
        select * from dna2
    ),
    _c1 as (
        select count(*) from _t1
    ),
    _c2 as (
        select count(*) from _t2
    )
select (select * from _c1) :: float / (select * from _c2);