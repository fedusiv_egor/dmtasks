import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Scanner;

public class Main {

    public static void sorting (String input, String output) throws FileNotFoundException {
        Scanner s = new Scanner(new File(input));
        HashMap<String, Long> data = new HashMap<>();
        PrintWriter pw = new PrintWriter(output);
        s.nextLine();
        while (s.hasNext()) {
            String[] lineInfo = s.nextLine().split(";");
            if (!data.containsKey(lineInfo[0])) data.put(lineInfo[0], (long)1);
            else data.put(lineInfo[0], data.get(lineInfo[0]) + 1);
        }
        data.entrySet()
            .stream()
            .sorted(HashMap.Entry.<String, Long>comparingByValue().reversed())
            .forEach(x -> pw.println(x.toString().split("=")[0] + "," + x.toString().split("=")[1]));
        pw.close();
    }

    public static void main(String[] args) throws FileNotFoundException {
        sorting("transactions.csv", "output.txt");
    }

}
