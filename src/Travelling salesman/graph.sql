create table graph (
    node_from varchar,
    node_to   varchar,
    cost      integer
);

insert into graph(node_from, node_to, cost) values ('U', 'V', 6), ('U', 'X', 2),
                                                   ('S', 'U', 3), ('S', 'X', 5),
                                                   ('X', 'U', 1), ('X', 'Y', 6), ('X', 'V', 4),
                                                   ('Y', 'S', 3), ('Y', 'V', 7),
                                                   ('V', 'Y', 2);

--all paths from 'S' to 'Y'

with recursive p(path, cycle, node_to, endY) as (
    select array[node_from] as path,
           false as cycle,
           node_to,
           false as endY
    from graph
    where node_from = 'S'
    union all
    select p.path || p.node_to,
           graph.node_to = any(p.path),
           graph.node_to,
           p.node_to = 'Y'
    from p, graph
    where p.node_to = graph.node_from and not cycle
)
select distinct path as paths from p where endY;

--paths from 'S' to 'Y' with cheapest price

with recursive p(path, cycle, node_to, endY, price) as (
    select array[node_from] as path,
           false as cycle,
           node_to,
           false as endY,
           graph.cost as price
    from graph
    where node_from = 'S'
    union all
    select p.path || p.node_to,
           graph.node_to = any(p.path),
           graph.node_to,
           graph.node_to = 'Y',
           p.price + graph.cost
    from p, graph
    where p.node_to = graph.node_from and not cycle
)
select distinct path || p.node_to as paths, price from p
where endY and price = (select min(price) from p where endY);