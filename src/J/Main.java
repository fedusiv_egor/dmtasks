import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {

    private static final String URL =
            "jdbc:postgresql://course-db.csqiiocqatup.us-east-1.rds.amazonaws.com:5432/postgres";
    private static final String USER = "postgres";
    private static final String PASSWORD = "sH28uu9oo#";

    private static final Function<Integer, String> SQL_INSERT_DNA =
            x -> "insert into dna" + x + "(piece) values (?)";
    private static final Function<Integer, String> SQL_TRUNCATE =
            x -> "truncate table dna" + x;


    private static Connection connection;

    public static void leftShift(int[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            arr[i] = arr[i + 1];
        }
    }

    public static void truncate(int table) {
        Statement statement = null;
        try {
            statement = connection.createStatement();
            statement.executeUpdate(SQL_TRUNCATE.apply(table));
            connection.commit();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
        }
    }

    public static void insertCodon(String codon, int table) {
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SQL_INSERT_DNA.apply(table));
            preparedStatement.setString(1, codon);
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        } finally {
            if (preparedStatement != null) try { preparedStatement.close(); } catch (SQLException e) {}
        }
    }

    public static void codonGetter(int table, String filePath, int length) throws IOException {
        FileReader fileReader = new FileReader(filePath);

        int[] buffer = new int[length];

        for(int i = 0; i < length; i++) {
            buffer[i] = fileReader.read();
        }

        int data = fileReader.read();
        while(data != -1) {
            String temp = Arrays.stream(buffer)
                    .mapToObj(x -> String.valueOf((char)x))
                    .reduce((acc, x) -> acc += x).get();
            insertCodon(temp, table);
            leftShift(buffer);
            if (data != (int)'G' && data != (int)'T' && data != (int)'C' && data != (int)'A') {
                data = fileReader.read();
            }
            buffer[buffer.length - 1] = data;
            data = fileReader.read();
        }
        fileReader.close();
    }

    public static void main(String[] args) throws SQLException, IOException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        connection = DriverManager.getConnection(URL, USER, PASSWORD);

        truncate(1);
        truncate(2);
        codonGetter(1, "gen1.txt", 3);
        codonGetter(2, "gen2.txt", 3);
        // J = (dna1 & dna2) / (dna1 | dna2)

    }

    private static RandomInRange<Integer> iRandom;
    private static Random rand = new Random();
    private static Function<Integer, String> strRandomGenerator;

    static {
        iRandom = (lower, upper) -> lower.intValue()
                + rand.nextInt(upper.intValue() - lower.intValue());

        strRandomGenerator =
                length -> { Character[] arr = new Character[length];
                    IntStream.range(0, length)
                            .forEach(x -> arr[x] = (char)(iRandom.apply((int)'a', (int)'z').intValue()));
                    return Arrays.stream(arr).map(String::valueOf)
                            .collect(Collectors.joining(""));
                };
    }

}